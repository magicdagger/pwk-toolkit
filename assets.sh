#!/bin/bash

# dirty hack to organize shit in a sqlite3 db

_db=${PWK_DB}
_ip=""
__ip="INIT"

[ -w ${_db} ] || exit 2;

while [ -z $_ip ]; do
	read -p "IP: " _ip
done
read -p "Hostname: " _hname
read -p "OS (e.g. Windows, Linux, FreeBSD): " _os
read -p "Version: " _ver
read -p "proof: " _proof

# update
echo "SELECT * FROM hosts WHERE ip=\"$_ip\";" | sqlite3 $_db | while IFS='|' read __ip __hname __os __ver __proof; do
	_hname=${_hname:-$__hname}
	_os=${_os:-$__os}
	_ver=${_ver:-$__ver}
	_proof=${_proof:-$__proof}
	cat <<EOF | sqlite3 ${_db}
UPDATE hosts SET ip="$_ip", hostname="$_hname", os="$_os", version="$_ver", proof="$_proof" WHERE ip="$_ip";
EOF
	printf "updated: %s %s %s %s %s\n" "$_ip" "$_hname" "$_os" "$_ver" "$_proof"
	exit 255
done

# insert
if [ $? -ne 255 ]; then
	cat <<EOF | sqlite3 ${_db}
INSERT INTO hosts (ip,hostname,os,version,proof) VALUES("$_ip","$_hname","$_os","$_ver","$_proof");
EOF
	printf "added: %s %s %s %s %s\n" "$_ip" "$_hname" "$_os" "$_ver" "$_proof"
fi

# dirty hack to have a small `web server' for loading exploits and
# such. Thanks to overly sensitive powershell this should be protocol
# compliant

_exit() {
	echo "Caught SIGINT. Exiting..."
	exit 0
}

_file=$1
_port=${2:-443}

if [ -z $_file ]; then
	cat <<EOF>&2
usage: $0 <file>
	<file>  file/script to serve
EOF
	exit 2
fi

_fn=$(basename ${_file})

trap _exit SIGINT 

while :; do
	printf "[*] Serving %s from http://*:%d/\n" "${_file}" "${_port}"
	{ 
		printf 'HTTP/1.1 200 OK\r\n'
		printf 'Server: ff shell httpd\r\n'
		printf 'Content-Length: %s\r\n' "$(wc -c $_file | cut -f1 -d' ')"
		printf 'Content-Type: application/octet-stream\r\n'
		printf 'Content-Disposition: attachment; filename="%s"\r\n' "$_fn"
		printf 'Content-Transfer-Encoding: binary\r\n\r\n'
		cat ${_file}
	} | nc -nvvvvlp ${_port} -q1
done

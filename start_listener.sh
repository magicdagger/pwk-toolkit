#!/bin/bash

# protect me from ^c

_argl() {
	stty intr '^c'
}

_usage() {
	cat <<EOF
usage: $0 [<port>|help]

	<port>	port to listen on. Default: 443
	help	this message
EOF
	exit 0
}

trap '' SIGINT
trap _argl EXIT

_port=${1:-443}

case ${_port} in
	*[!0-9]*)	_usage;;	
esac

printf "*** don't fuck yourself with habits.       ***\n"
printf "*** ctl-c disabled, use ctl-_ instead bro. ***\n"
stty intr '^_'
nc -nlvp ${_port}
